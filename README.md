# Awesome

## What is it actually
Apparently I tried to turn the Awesome window manager into a usable desktop environment. Hope you will like it.

## Dependencies
- awesome
- polybar
- nitrogen
- network-manager-applet
- ntfd-bin (https://github.com/kamek-pf/ntfd)

## How to install
- Open your terminal
- Run the command
- Don't forget to type your super secret **Password** press **Enter** when you're asked to
- That's it! Enjoy your **Awesome** desktop environment.

```
mkdir -p ~/.config/awesome ; cd ~/.config/awesome ; git clone https://gitlab.com/safwannayeem569/awesome.git ; cd ~/.config/awesome/awesome ; chmod +x awesomedesktopenvironment.sh ; sh awesomedesktopenvironment.sh
```


